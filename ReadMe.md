This java-maven project is developed for screening process of New Motion QA Lead position.

**Prerequisites**

Java 15 and Maven 3 are required in order to run the project manually.


**Purpose**

This java-maven project is developed for New Motion Screening Process. Here are the details of rest assured web service test.

SWAPI is selected as test api provider.



**Test Scenario**

There are five test scenarios exercising Star Wars Api. Those are;

```
aNewHopeReleaseDateTest
```

```
obiWanKenobiFilmsCountTest
```

```
starshipsTotalCountTest
```

```
firstStarshipListingTest
```

```
deathStarAttributesTest
```


**Test Execution**

Test can ben run through "TestNG.xml" and maven build (selecting only integration tests) using the following command.

```
mvn test -DsuiteXmlFile=TestNG.xml -Denv=production
```


**Test Reporting**

Extent reports is used for test reporting. Each run creates an html file under "test-output" folder with timestamp.


**GitLab CI**

Project is configured to run on GitLab CI using maven:latest image from Docker Hub. After compilation in build stage, StarWars API tests are run using RestAssured in test stage. Test reports created by GitLab CI can be seen under "Job artifacts" section of run job through webui or https://gitlab.com/test7018/NewMotionAssignment/-/jobs/**$JOB_ID**/artifacts/browse



