package StarWarsApi.Utility;

import com.relevantcodes.extentreports.ExtentReports;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ExtentManager {
    static ExtentReports extent;
    static String timeStamp = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss").format(new Date());
    final static String filePath = System.getProperty("user.dir") +"/test-output/NewMotionAssignmentTestResults "+ timeStamp +".html";

    public synchronized static ExtentReports getReporter() {
        if (extent == null) {
            extent = new ExtentReports(filePath, true);
        }
        return extent;
    }
}