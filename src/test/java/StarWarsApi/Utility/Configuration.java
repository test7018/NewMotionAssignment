package StarWarsApi.Utility;

import org.apache.commons.lang3.StringUtils;

import java.util.Properties;

public class Configuration {
    private String  baseUrl;
    private Integer numberOfStarships;
    private Integer numberofObiWanKenobiFilms;

    public Configuration() {
        try {
            String env = System.getProperties().getProperty("env");
            if(StringUtils.isBlank(env) ){
                env="production";
                System.out.println("No ENV option is set, please set -Denv in java. Default is set to production");
            }

            Properties configProps = new Properties();
            configProps.load(ClassLoader.getSystemResourceAsStream(env + "_config.properties"));

            this.baseUrl= configProps.getProperty("base.url");
            this.numberOfStarships= Integer.parseInt(configProps.getProperty("number.of.starships"));
            this.numberofObiWanKenobiFilms = Integer.parseInt(configProps.getProperty("number.of.obiwankenobi.films"));

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public String getBaseUrl() {return baseUrl;}

    public Integer getNumberOfStarships() {return numberOfStarships;}

    public Integer getnumberofObiWanKenobiFilms() {return numberofObiWanKenobiFilms;}
}
