package StarWarsApi.Test;

import StarWarsApi.Utility.BaseTest;
import io.restassured.response.ValidatableResponse;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.Matchers.hasItem;

public class FilmsApiTest extends BaseTest {
    @Test
    public void aNewHopeReleaseDateTest() {
        ValidatableResponse res = given()
                .when().get(configuration.getBaseUrl() +
                        "films/" +
                        "?search=hope")
                .then().assertThat()
                .contentType(JSON)
                .body("results.title", hasItem("A New Hope"))
                .body("results.release_date", hasItem("1977-05-25"))
                .statusCode(200);
    }
}
