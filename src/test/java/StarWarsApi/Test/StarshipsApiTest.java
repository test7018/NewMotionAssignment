package StarWarsApi.Test;

import StarWarsApi.Utility.BaseTest;
import io.restassured.response.ValidatableResponse;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.Matchers.*;
import static org.testng.Assert.assertEquals;


public class StarshipsApiTest extends BaseTest {
    @Test
    public void starshipsTotalCountTest() {
        ValidatableResponse response = given()
                .when().get(configuration.getBaseUrl() +
                        "starships/")
                .then().assertThat()
                .contentType(JSON)
                .body("count", equalTo(configuration.getNumberOfStarships()))
                .statusCode(200);
    }

    @Test
    public void firstStarshipListingTest() {
        ValidatableResponse response = given()
                .when().get(configuration.getBaseUrl() +
                        "starships/1")
                .then().assertThat()
                .contentType(JSON)
                .statusCode(200);
    }

    @Test
    public void deathStarAttributesTest() {
        ValidatableResponse response = given()
                .when().get(configuration.getBaseUrl() +
                        "starships/" +
                        "?search=Death Star")
                .then().assertThat()
                .contentType(JSON)
                .body("results.name", hasItem("Death Star"))
                .body("results.model", hasItem("DS-1 Orbital Battle Station"))
                .body("results.manufacturer", hasItem("Imperial Department of Military Research, Sienar Fleet Systems"))
                .statusCode(200);
    }
}


