package StarWarsApi.Test;

import StarWarsApi.Utility.BaseTest;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.Matchers.hasItem;
import static org.testng.Assert.assertEquals;

public class PeoplesApiTest extends BaseTest {
    @Test
    public void obiWanKenobiFilmsCountTest() {
        Response res = given()
                .when().get(configuration.getBaseUrl() +
                        "people/" +
                        "?search=Obi-Wan Kenobi")
                .then().assertThat()
                .contentType(JSON)
                .body("results.name", hasItem("Obi-Wan Kenobi"))
                .statusCode(200)
                .extract().response();

        assertEquals(configuration.getnumberofObiWanKenobiFilms(), res.jsonPath().getList("results[0].films").size());
    }
}
